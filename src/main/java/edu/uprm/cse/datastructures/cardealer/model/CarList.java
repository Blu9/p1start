package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

public class CarList
{

	private static CarList singleton = new CarList();
	
	static CircularSortedDoublyLinkedList<Car> CarList = new CircularSortedDoublyLinkedList<Car>(new CarComparator());
	
	private CarList()
	{
		CarList = new CircularSortedDoublyLinkedList<Car>(new CarComparator());
	}
	
	public static CircularSortedDoublyLinkedList<Car> getInstance()
	{
		return CarList;
	}

	public static void resetCars()
	{
		CarList = new CircularSortedDoublyLinkedList<Car>(new CarComparator()); 
	}

}
