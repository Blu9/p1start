package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarComparator implements Comparator<Car>
{

	@Override
	public int compare(Car car, Car car2) {
		String tempCar = car.getCarBrand() + car.getCarModel() + car.getCarModelOption();
		String tempCar2 = car2.getCarBrand() + car2.getCarModel() + car2.getCarModelOption();
		
		return tempCar.compareTo(tempCar2);
	}
	
}
