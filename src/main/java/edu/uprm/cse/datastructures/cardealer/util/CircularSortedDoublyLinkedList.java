package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class CircularSortedDoublyLinkedList<E> implements SortedList<E> {


	private static class Node<E>    
	{
		private E value;
		private Node<E> next, prev;


		public Node()
		{
			super();
		}
		public Node(E v, Node<E> n, Node<E> p)
		{
			value = v;
			next = n;
			prev = p;
		}
		public void setNext(Node<E> n)
		{
			next = n;
		}
		public void setPrev(Node<E> p)
		{
			prev = p;
		}    
		public Node<E> getNext()
		{
			return next;
		}
		public Node<E> getPrev()
		{
			return prev;
		}
		public void setValue(E v)
		{
			value = v;
		}
		public E getValue()
		{
			return value;
		}
	}

	private Node<E> header;
	private Comparator<E> comparator;
	private int currentSize;

	public CircularSortedDoublyLinkedList()
	{
		this.header = new Node<E>();
		this.currentSize = 0;
	}

	public CircularSortedDoublyLinkedList(Comparator<E> comparator)
	{
		this.comparator = comparator;
		this.header = new Node<E>();
		this.currentSize = 0;
	}

	@Override
	public boolean add(E obj)
	{
		boolean itWorked = false;

		Node<E> nodeToAdd = new Node<E>(obj, null, null);

		if(obj.equals(null))
		{
			throw new IllegalArgumentException();
		}

		if(currentSize == 0)
		{
			this.header.setNext(nodeToAdd);
			this.header.setPrev(nodeToAdd);
			nodeToAdd.setNext(header);
			nodeToAdd.setPrev(header);
			this.currentSize++;
			itWorked = true;
			return itWorked;
		}
		else
		{
			Node<E> currNode = this.header.getNext();
			while(currNode != header)
			{
				if(comparator.compare(nodeToAdd.getValue(), currNode.getValue()) < 0)
				{
					currNode.getPrev().setNext(nodeToAdd);
					nodeToAdd.setPrev(currNode.getPrev());
					nodeToAdd.setNext(currNode);
					currNode.setPrev(nodeToAdd);
					itWorked = true;
					currentSize++;
					break;
				}
				currNode = currNode.getNext();
			}
			if(itWorked == false)
			{
				nodeToAdd.setPrev(header.getPrev());
				nodeToAdd.setNext(header);
				header.getPrev().setNext(nodeToAdd);
				header.setPrev(nodeToAdd);
				itWorked = true;
				currentSize++;
			}
			return itWorked;
		}
	}

	@Override
	public int size()
	{
		return this.currentSize;
	}

	@Override
	public boolean remove(E obj)
	{
		if(obj.equals(null))
		{
			throw new IllegalArgumentException();
		}

		for (Node<E> temp = this.header.getNext(); temp != this.header; temp = temp.getNext())
		{
			if(temp.getValue().equals(obj))
			{
				temp.getPrev().setNext(temp.getNext());

				temp.getNext().setPrev(temp.getPrev());

				temp.setNext(null);
				temp.setPrev(null);
				temp.setValue(null);

				this.currentSize--;
				return true;
			}
		}

		return false;

	}

	@Override
	public boolean remove(int index)
	{
		if(index < 0 || index > this.currentSize)
		{
			throw new IndexOutOfBoundsException();
		}

		return this.remove(this.get(index));
	}

	@Override
	public int removeAll(E obj)
	{
		int removedCount = 0;
		while(this.contains(obj))
		{
			this.remove(obj);
			removedCount++;
		}

		return removedCount;
	}

	@Override
	public E first()
	{
		if (this.isEmpty()) {return null;}
		return header.getNext().getValue();
	}

	@Override
	public E last()
	{
		if(isEmpty()) {return null;}
		return header.getPrev().getValue();
	}

	@Override
	public E get(int index)
	{
		if(index < 0 || index > this.currentSize) {throw new IndexOutOfBoundsException();}

		int currIndex = 0;
		for (Node<E> temp = this.header.getNext(); temp != this.header; temp = temp.getNext())
		{
			if(currIndex == index)
			{
				return temp.getValue();
			}
			currIndex++;
		}
		return null;
	}

	@Override
	public void clear()
	{
		while(!isEmpty()) {this.removeAll(header.getNext().getValue());}
	}

	@Override
	public boolean contains(E e)
	{
		for (Node<E> temp = this.header.getNext(); temp != this.header; temp = temp.getNext())
		{
			if(temp.getValue().equals(e))
			{
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean isEmpty()
	{
		return this.currentSize == 0;
	}

	@Override
	public int firstIndex(E e)
	{
		int currIndex = 0;
		for (Node<E> temp = this.header.getNext(); temp != this.header; temp = temp.getNext())
		{
			if(temp.getValue().equals(e))
			{
				return currIndex;
			}
			currIndex++;
		}
		return -1;
	}

	@Override
	public int lastIndex(E e)
	{
		int currIndex = this.size()-1;
		for (Node<E> currNode = this.header.getPrev(); currNode != this.header; currNode = currNode.getPrev())
		{
			if(currNode.getValue().equals(e))
			{
				return currIndex;
			}

			currIndex--;
		}
		return -1;

	}

	private class ListIterator implements Iterator<E>{
		private Node<E> nextNode;

		public ListIterator(){
			this.nextNode = header.getNext();
		}

		@Override
		public boolean hasNext() {
			return nextNode.getValue() != null;
		}

		@Override
		public E next() {
			if (hasNext()){
				E result = this.nextNode.getValue();
				this.nextNode = this.nextNode.getNext();
				return result;
			}
			else {
				throw new NoSuchElementException();
			}
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();

		}	
	}

	@Override
	public Iterator<E> iterator() {
		return new ListIterator();
	}
}

