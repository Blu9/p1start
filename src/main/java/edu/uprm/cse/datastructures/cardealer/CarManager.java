package edu.uprm.cse.datastructures.cardealer;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.util.Optional;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarList;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;


@Path("/cars")
@Produces(MediaType.APPLICATION_JSON)
public class CarManager
{
	private final CircularSortedDoublyLinkedList<Car> cList = CarList.getInstance(); 

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars()
	{
		if(cList.isEmpty())
		{
			return new Car[1];
		}
		else
		{
			Car[] list = new Car[cList.size()];
			for(int i = 0; i < cList.size(); i++)
			{
				list[i] = cList.get(i);
			}
			return list;
		}
	}

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCustomer(@PathParam("id") int id)
	{
		for (int i = 0; i < cList.size(); i++)
		{
			if(cList.get(i).getCarId() == id)
			{
				return cList.get(i);
			}
		}

		throw new WebApplicationException(404);

	}

	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car car) 
	{
		cList.add(car);
		return Response.status(201).build();

	}

	@PUT
	@Path("{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(Car car) 
	{
		for (int i = 0; i < cList.size(); i++)
		{
			if (cList.get(i).getCarId() == car.getCarId())
			{
				cList.remove(cList.get(i));
				cList.add(car);
				return Response.status(Response.Status.OK).build();
			}

		}
		return Response.status(Response.Status.NOT_FOUND).build();

	}

	@DELETE
	@Path("/{id}/delete")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteCar(@PathParam("id") long id) 
	{
		for (int i = 0; i < cList.size(); i++)
		{
			if(cList.get(i).getCarId() == id)
			{
				cList.remove(cList.get(i));
				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build(); 
	}
}
